class GameAcak
  @@total_skor = 0
  def game
    kata = ["buku", "pensil", "mobil", "komputer", "penghapus", "penggaris", "sepatu", "telepon seluler", "jam", "meja", "pendingin ruangan", "pemanas air", "kursi", "sandal", "tas", "sepeda motor", "sepeda", "bola"]
    loop do
      begin
        kata_diambil = kata.sample
        kata_acak = kata_diambil.to_s.split('').shuffle.join
      end while kata.include? kata_acak
      puts "Tebak Kata: " + kata_acak
      print "Jawab: "
      kata_tebakan = gets.chomp
      if kata_tebakan == 'exit'
        puts "Total Point Anda : #{@@total_skor}"
        puts "Terima Kasih sudah bermain."
        exit()
      else
        if kata_diambil == kata_tebakan
          print "BENAR "
          score()
          puts " "
        else
          puts "SALAH! Silahkan coba lagi"
          puts " "
        end
      end
    end
  end

  def score()
    @@total_skor += 1
    return puts "point anda : #{@@total_skor}!"
  end
end

if __FILE__ == $0
  gm = GameAcak.new
  puts "Selamat Datang di Permainan Acak Kata"
  puts "Ketik exit untuk keluar permainan."
  puts " "
  gm.game
end
